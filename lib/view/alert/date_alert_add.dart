import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:zhaoxiaowu_app/eventbus/event_bus.dart';
import 'package:zhaoxiaowu_app/global/global.dart';
import 'package:zhaoxiaowu_app/utils/date_utils.dart';

class DateAlertAdd extends StatefulWidget {
  const DateAlertAdd({Key key}) : super(key: key);

  @override
  _DateAlertAddState createState() => _DateAlertAddState();
}

class _DateAlertAddState extends State<DateAlertAdd> {
  TextEditingController _name;
  TextEditingController _day;
  String _date;
  int _message = 0;
  int _type = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _name = TextEditingController();
    _day = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _name.dispose();
    _day.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          leading: Text(
            "日期名称",
            style: TextStyle(fontSize: 18),
          ),
          title: Container(
            child: TextField(
              controller: _name,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "请输入日期名称",
              ),
            ),
          ),
        ),
        Divider(height: 1),
        ListTile(
          leading: Text(
            "提醒日期",
            style: TextStyle(fontSize: 18),
          ),
          title: GestureDetector(
            child: Text(_date == null || _date.isEmpty ? "请选择日期" : _date),
            onTap: _showDateAlert,
          ),
        ),
        Divider(height: 1),
        ListTile(
          leading: Text(
            "提示天数",
            style: TextStyle(fontSize: 18),
          ),
          title: Container(
            width: double.infinity,
            child: TextField(
              controller: _day,
              decoration: InputDecoration(
                border: InputBorder.none,
                hintText: "请输入提示天数",
              ),
            ),
          ),
        ),
        Divider(height: 1),
        Row(
          children: [
            SizedBox(width: 16),
            Text("是否开启推送"),
            Checkbox(
              value: _message == 0 ? true : false,
              onChanged: (v) {
                setState(() {
                  _message = v ? 0 : 1;
                });
              },
            ),
            SizedBox(width: 32),
            Text(_type == 0 ? "阳历" : "阴历"),
            Checkbox(
              value: _type == 0 ? true : false,
              onChanged: (v) {
                setState(() {
                  _type = v ? 0 : 1;
                });
              },
            ),
          ],
        ),
        Divider(height: 1),
        Container(
          padding: EdgeInsets.only(
            left: 16,
            right: 16,
          ),
          width: double.infinity,
          child: RaisedButton.icon(
            onPressed: _submit,
            icon: Icon(Icons.add),
            label: Text("提交"),
          ),
        ),
      ],
    );
  }

  void _submit() async {
    if (_name.text.isEmpty) {
      EasyLoading.showError("日期名称不能为空！");
      return;
    }
    var result = await Global.getInstance().dio.post(
      "/zxw/DateMessage",
      queryParameters: {
        "json": json.encode({
          "name": _name.text,
          "alert_date": _date,
          "message": _message,
          "message_day": _day.text,
          "type": _type,
        }),
      },
    );
    print(result);
    if (result.data["success"]) {
      bus.emit("date");
      bus.emit("date_list");
    } else {
      EasyLoading.showError(result.data["msg"]);
    }
  }

  void _showDateAlert() async {
    DateTime result = await showDialog(
      context: context,
      builder: (context) {
        return DatePickerDialog(
          restorationId: 'date_picker_dialog',
          initialEntryMode: DatePickerEntryMode.calendarOnly,
          initialDate: DateTime.now(),
          firstDate: DateTime(1090, 1, 1),
          lastDate: DateTime(2050, 1, 1),
        );
      },
    );
    print(result);
    setState(() {
      _date = getYMD(result);
    });
  }
}
