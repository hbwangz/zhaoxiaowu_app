import 'package:flutter/material.dart';
import 'package:zhaoxiaowu_app/eventbus/event_bus.dart';
import 'package:zhaoxiaowu_app/view/alert/date_alert_add.dart';
import 'package:zhaoxiaowu_app/view/alert/date_alert_list.dart';

class DateAlertMenu extends StatefulWidget {
  const DateAlertMenu({Key key}) : super(key: key);

  @override
  _DateAlertMenuState createState() => _DateAlertMenuState();
}

class _DateAlertMenuState extends State<DateAlertMenu>
    with SingleTickerProviderStateMixin {
  List<Widget> widgets = [DateAlertList(), DateAlertAdd()];
  TabController _controller;

  @override
  void initState() {
    _controller = new TabController(length: widgets.length, vsync: this);
    bus.on("date", (arg) {
      _controller.animateTo(0);
    });
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    bus.off("date");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("日期提醒"),
        centerTitle: true,
        elevation: 10,
        bottom: TabBar(
          controller: _controller,
          tabs: [
            Tab(
              text: "列表",
            ),
            Tab(
              text: "新增",
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _controller,
        children: widgets,
      ),
    );
  }
}
