import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:zhaoxiaowu_app/eventbus/event_bus.dart';
import 'package:zhaoxiaowu_app/global/global.dart';
import 'package:zhaoxiaowu_app/model/accouting_add_model.dart';
import 'package:zhaoxiaowu_app/utils/alert_utils.dart';
import 'package:zhaoxiaowu_app/utils/date_utils.dart';

class LoveImageViewmodel extends ChangeNotifier {
  List _imgs;

  List get getImgs {
    return _imgs;
  }

  void setImgs(List val) {
    _imgs = val;
    notifyListeners();
  }

  void loadData() async {
    var result = await Global.getInstance().dio.get("/zxw/Imgs");
    print(result.data);
    if (result.data["success"]) {
      setImgs(result.data["data"]);
    } else {
      EasyLoading.showError(result.data["msg"]);
    }
  }

  void delete(String id) async {
    var result = await Global.getInstance().dio.delete(
      "/zxw/Imgs",
      data: {
        "id": id.toString(),
      },
    );
    if (result.data["success"]) {
      EasyLoading.showSuccess(result.data["msg"]);
      loadData();
    } else {
      EasyLoading.showError(result.data["msg"]);
    }
  }
}
